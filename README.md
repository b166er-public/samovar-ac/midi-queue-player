![MQP Icon](doc/resources/samovar_icon_full_inverted.png)

# MQP (MIDI queue player)

The following piece of software is a tool I wrote to be able to focus on the main topic I am currently working on - 
algorithmic composition.

When I start thinking about how I can focus on the generation of compositions and do not bother about the question
how the composition will later land on the MIDI bus which is connected to my synthesizer I identify the need for an
abstraction layer which could do all the tasks for me.

I thought it would be cool if I do not have to think about MIDI at all and simply define my composition more or less like
I would do this within an ordinary step sequencer. To avoid working with MIDI directly when thinking about the **fun** part
of algorithmic composition I tried to find a format that is so simple that everyone with a basic understanding of how
the computer works could use it - regardless of which language should be used and which CPU architecture is available.
This format I call simply **seq** and I will introduce it later in this document.

To avoid bothering with different platforms I decided to use the technology I am currently most familiar with - Java.
For the use-cases which are relevant here this technology is absolutely enough - though I am sure that others would be
more optimal regarding resource consumption.

When working with MQP I thought about the following workflow:
1) Some piece of software generates a **seq** or a **MIDI** file which contains a snippet of a composition
2) MQP receives this file using ...
   - file system monitoring (e.g. wait until a new file appears in a specified folder)
   - the file is sent via HTTP POST to an appropriate endpoint of MQP
   - the file is sent via ZMQ to an appropriate response endpoint of MQP
3) If MQP accepts this file it will convert it to a proper MIDI file
4) MQP will send the MIDI file to an arbitrary combination of these sources:
   - folder somewhere on your file system
   - a ZMQ response endpoint
   - a MIDI Interface attached to the system where the MQP is running 

## Architecture
 
![Architecture of MQP](doc/resources/architecture.png)

## Installation

As the complete application is written in pure Java there is actually no need to install this application. 

In case you belong to those people who secured your OS properly, you can visit the release page of this repository to download
the version of MQP, that fits for you (latest should be always good candidate)

[Release Page](https://gitlab.com/b166er-public/samovar-ac/midi-queue-player/-/releases/)

In case you suffer a bit from paranoia OR just want to try own own things with MQP which goes beyond simple usage, go on
and read how to build MQP.

## Build instruction

To build this application I use the common tooling for Java which is Maven. Hereby I will explain what to do on a Linux 
system (as it is the only OS I have in my near surrounding). If someone wants to add more OS specific instructions, simply
create a merge request and I will merge it after a short review.

In case you do not have git installed, please execute the following line ...

```bash
sudo apt-get install git-all
```

In case you do not have Java installed please execute the following line ...

```bash
sudo apt-get install openjdk-11-jdk
```

You will need the JDK as you will later build this application on your machine. After the JDK is installed let us install
Maven by executing the following ...

```bash
sudo apt install maven
```

If that went without any errors, your system should have everything what you need to build MQP. Well, actually just one
thing is missing ... the source code. To get it from Gitlab go to a folder where you want the repository to be cloned and
execute following line ...

```bash
git clone git@gitlab.com:samovar-ac/midi-queue-player.git
```

Change in your local copy of the resository by ...

```bash
cd midi-queue-player
```

Now let's build MQP. Simply execute following line ...

```bash
mvn -P fatjar clean install
```

If your last lines looks more like this ...

```bash
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  3.049 s
[INFO] Finished at: 2021-07-15T00:58:07+02:00
[INFO] ------------------------------------------------------------------------
```
... than everything is fine and MQP had been build successfully. The binary you will find in the relative folder ...

```bash
cd ./target/dist-fatjar/
```

It should have a name like ```midi-queue-player-1.0.0.jar```. This is the binary where you find all features implemented
by MQP. To have a shorter command I rename the jar file to ```mqp.jar```. This is not mandatory but please keep this in mind
as all further commands will assume the usage of this short name. To get a first look on this application execute following line ...

```bash
java -jar mqp.jar
```

The output should look like this ...

```bash
_|      _|     _|_|       _|_|_|
_|_|  _|_|   _|    _|     _|    _|
_|  _|  _|   _|  _|_|     _|_|_|
_|      _|   _|    _|     _|
_|      _|     _|_|  _|   _|

Written by Daniil Moerman
Version: 1.0.0


Usage: mqp [COMMAND]
(M) MIDI (Q) queue (P) player
Commands:
  env          List all mqp relevant environment variables
  device-list  List all MIDI devices accessible by your system
  run          Will run the midi-queue-player

Process finished with exit code 0
```

## Commands

- **env**
  - List all environment variables which are used for parametrization of MQP
- **device-list**
  - List all MIDI devices connected to this system
- **run**
  - Start the main feature of MQP which is taking compositions and transform/forward them to targets
   
## Parametrization

As I do not really like the ambiguities which comes with typical CLI argument passing I decided to replace CLI arguments
with environment variables. That means that if you want to pass MQP a special argument you instead look up the correct
name of an environment variable and set it accordingly.

Following environment parameters are currently suppored:

### Generic configuration
**MQP_TMP_FOLDER_PATH** Location where intermediate files should be stored

**MQP_MIN_MIDI_RESOLUTION**  The minimal MIDI file resolution (ticks per beat).

### Input Layer - File System
**MQP_INPUT_FS_ACTIVATED** Should monitoring of file-system be activated?

**MQP_INPUT_FS_FOLDER_PATH** Which folder should be monitored for playback material?

### Input Layer - HTTP
**MQP_INPUT_HTTP_ACTIVATED** Should monitoring of HTTP endpoint be activated?

**MQP_INPUT_HTTP_IP** IP address which the HTTP endpoint should be bound to.

**MQP_INPUT_HTTP_PORT** Port which the HTTP endpoint should be bound to.

### Input Layer - ZMQ
**MQP_INPUT_ZMQ_ACTIVATED** Should monitoring of ZMQ endpoint be activated?

**MQP_INPUT_ZMQ_IP** IP address which the ZMQ endpoint should be bound to.

**MQP_INPUT_ZMQ_PORT** Port which the ZMQ endpoint should be bound to.

### Output Layer - File System
**MQP_OUTPUT_FS_ACTIVATED** Should export to file-system be activated?

**MQP_OUTPUT_FS_FOLDER_PATH** Which folder should be used for export of received playback material?

### Output Layer - MIDI
**MQP_OUTPUT_MIDI_ACTIVATED** Should export to MIDI OUT be activated?

**MQP_OUTPUT_MIDI_DEVICE_ID** Id of the MIDI device which will be used for playback.

### Output Layer - ZMQ
**MQP_OUTPUT_ZMQ_ACTIVATED** Should export to ZMQ endpoint be activated?

**MQP_OUTPUT_ZMQ_IP** IP address where the ZMQ endpoint can be reached.

**MQP_OUTPUT_ZMQ_PORT** Port where the ZMQ endpoint can be reached.

## Supported file formats

Regardless which trajectory you use to get your composition in MQP, you can use following file formats:
- ```*.mid``` or ```*.midi``` for ordinary MIDI files
- ```*.seq``` for sequence files
- ```*.seq.gz``` for gzipped sequence files

## Get list of connected MIDI interfaces

To get a list of connected and supported MIDI interfaces, simply execute following

```bash
java -jar mqp.jar device-list
```

If you see the interface you want to use later, just remember the ID as this will be needed later for configuration

## Sequence format

As mentioned already multiple times I decided to create a new format for allow easy creation of compositions. The structure
of this sequence format is relatively easy. It is a simple byte array which has a **header** section and a **body** section.

### File structure - Semantic level

![seq file structure - semantic level](doc/resources/seq-semantic.png)

### File structure - Representation level

![seq file structure - representation level](doc/resources/seq-structure.png)

The header has always 5 bytes which have following semantic:
- Byte 0 : ASCII value of **s** = **0x73** 
- Byte 1 : ASCII value of **e** = **0x65**
- Byte 2 : ASCII value of **q** = **0x71**
- Byte 3 : A value between 1 ... 255 which defines the **BPM** (beats per minute)
- Byte 4 : A value between 1 ... 255 which defines how much steps a beat will consist of

The body of a seq file is continuous array which consist of block with ```16 * 128``` bytes. The reason for this size is
that every cell is representing the velocity value of a key at a certain MIDI channel in a single step.

### File structure - ASCII representation

Originally the seq-Format should be a binary format which is optimized for easy generation using any kind of turing-complete
programming language. Later some smart people from a very cool chat channel I have the honor to be part of inspired me to
think about following use-case:

Some people meet each other in a chat-room. This people have the possibility to listen more or less in realtime what comes
out from a synthesizer (or even a complete set of sythensizers) which is physically located somewhere else in this world. 
This people would like to try out some compositions using this instrument. To do so they simply post a message in the chat-room.
This message contains a unambigious description of the composition one of the participants have in mind. Meanwhile, one of
the member of this char-room is not actually human and simply waits for messages of this kind and forward them via MIDI to
the synthesizer.

To make long story short ... here is the textual format I thought about in this very moment. I think it is not the ultimative
optimum and as most solutions is merely a local optimum, but it is definitely something we could start with.

To introduce the format, lets simply start with an example:

```text
ascii-seq 60-32-4 t0d32c0k40v100 t32k42 t64k45
```

This is what I call the ASCII representation of a seq-File and it should also be stored with the ```.seq``` extension.
Every whitespace is seperating tokens from each other. That way we can see here following tokens:

* ```ascii-seq```
  - Can be seen like a magic number. Every seq-File in ASCII representation starts with this content
* ```60-32-4```
  - Means that the following composition will be at 60 BPM, where each beat will be seperated in 32 steps
  - The complete composition will last exactly 4 beats
* ```t0d32c0k40v100```
  - A note should be played beginning at step 0 ```t0```
  - Last for 32 steps (or in this context a single beat) ```d32```
  - Should be played on MIDI channel 0 ```c0```
  - Should has the pitch of MIDI key number 40 ```k40```
  - Should has the velocity (in MIDI sense) of 100 ```v100```
* ```t32k42```
  - An another note should be played beginning at step 32 ```t32```
  - The duration is not specified what always mean, take the last know value which was 32 from last note
  - The channel is also not set, here the same strategy and we get MIDI channel 0
  - The MIDI key which should be played here is 42 ```k42```
  - The velocity is not specified and thus will also be defined by last known value 100
* ```t64k45```
  - Here the sample game as in previous note. Start time is the 64th step ```t64```
  - Duration is taken from the last know value, which is 32 steps
  - MIDI channel is also derived from last know value, which means 0
  - The key is specified explicit as 45 ```k45```
  - Velocity is again taken from last know value, which is 100
    
There is nothing more to say here. That is it for now and I hope it is enough to get the first impression of the
capabilities of the ASCII representation of the seq-Format. I think the format is fairly limited in comparison with MIDI
but also is adequate for the use-case which is in focus here.

### Timing in seq file

![seq timing](doc/resources/seq-time.png)

A time unit on seq file level is called a **step**. A step is always an integer amount of MIDI ticks. That is also the reason, why you always configure the minimum MIDI resolution for MQP - if the step do not fit exactly to a whole number of MIDI ticks, the resolution is adapted automatically. 

The **spb** steps-per-beat parameter specify how many steps will result in a single MIDI beat/quater-note.

The velocity specified in a cell will be valid right after the beginning of a step!

### Example
The following small python code snippet generates a composition which will contain 8 beats which will be splited into 32
steps each. The BPM will be 60. The composition will be played on MIDI channel 1 and will play the notes 50,55,50,55,12,20,80,51

```python
import numpy as np

bpm = 60
spb = 32
beats = 8
keys = [50,55,50,55,12,20,80,51]
channel = 1

steps = beats * spb 

seq = np.zeros(shape=(steps,16,128), dtype=np.uint8)

for b in range(beats):
    for s in range(spb):
        step = b * spb + s
        key = keys[b % len(keys)]
        seq[step,channel,key] = 100
        
bodyBytes = seq.tobytes()

magicBytes = bytes([0x73,0x65,0x71])
bpmBytes = bytes([bpm])
spbBytes = bytes([spb])
headerBytes = magicBytes + bpmBytes + spbBytes

seqBytes = headerBytes + bodyBytes
```
The variable ```seqBytes``` will contain the byte-array which represents the complete composition.

## Send composition via HTTP

If you activated the HTTP monitoring endpoint of MQP than following code snippet in python will send your composition to
the MQP in case your target device (that device which is directly connected to the relevant MIDI bus) has the IP 192.168.177.30
and the HTTP endpoint is listening on port 32132.

```python
url = 'http://192.168.177.30:32132'
req.post(url, data = seqBytes)
```

Following curl call will send your composition using same procedure.

```shell
curl -X POST --data-binary @./sequence01.mid http://192.168.177.30:32132/
```

## Send composition via ZMQ

Following python code snippet shows how to use ZMQ for sending a composition:
In this case we use the same IP as above but the port of ZMQ we set to ```555555```

```python
context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect("tcp://192.168.177.30:55555")

for i in range(1):
    socket.send(seqBytes)

socket.close()
context.term()
```

## Send via file system

You also can trigger MQP by adding new files to a user-defined folder. Please note that in this situation it is highly recommended
to do the following:

1) Create a file which will contain the content of your composition in the target-folder, but give it a ```.tmp``` extension.
2) As soon as the composition file is written and closed, rename it to the correct extension.

# Examples

Now I will show you some examples how you could start MQP

## Example 1 : Monitor local file system and playback using MIDI
I want that as soon as a new composition file (*.mid, *.seq, *.seq.gz) is found in the folder ```/home/me/compositions/```
that it is immediately played back on MIDI device 12345. The temporal resolution of the resulting MIDI file should be at
least 256 ticks per beat.

```bash
export MQP_MIN_MIDI_RESOLUTION=256

export MQP_INPUT_FS_ACTIVATED=TRUE
export MQP_INPUT_FS_FOLDER_PATH=/home/me/compositions/

export MQP_OUTPUT_MIDI_ACTIVATED=TRUE
export MQP_OUTPUT_MIDI_DEVICE_ID=12345

java -jar mqp.jar run
```

## Example 2 : HTTP POST to MIDI device
I want that as soon as I receive a new composition file to my IP address 192.168.70:30 at TCP port 8080, I want it
immediately to be played back on MIDI device 43443. The temporal resolution of the resulting MIDI file should be at
least 128 ticks per beat.

```bash
export MQP_MIN_MIDI_RESOLUTION=128

export MQP_INPUT_HTTP_ACTIVATED=TRUE
export MQP_INPUT_HTTP_IP=192.168.70:30
export MQP_INPUT_HTTP_PORT=8080

export MQP_OUTPUT_MIDI_ACTIVATED=TRUE
export MQP_OUTPUT_MIDI_DEVICE_ID=43443

java -jar mqp.jar run
```

## Example 3 : I want to get a MIDI file from your ```seq``` format
I want that as soon as a new composition file (*.mid, *.seq, *.seq.gz) is found in the folder ```/home/me/in/```
that its MIDI representation is stored to ```/home/me/out/```. Additionally I would listen to the MIDI file by sending
the events to the MIDI device 332211. The temporal resolution of the resulting MIDI file should be at
least 32 ticks per beat.

```bash
export MQP_MIN_MIDI_RESOLUTION=32

export MQP_INPUT_FS_ACTIVATED=TRUE
export MQP_INPUT_FS_FOLDER_PATH=/home/me/in/

export MQP_OUTPUT_MIDI_ACTIVATED=TRUE
export MQP_OUTPUT_MIDI_DEVICE_ID=332211

export MQP_OUTPUT_FS_ACTIVATED=TRUE
export MQP_OUTPUT_FS_FOLDER_PATH=/home/me/out/

java -jar mqp.jar run
```


## Example N : Conclusion

Currently there are ```3*3*3 = 27``` cases which should be handled by MQP as it knows 3 differet sources, can work with 3
different formats and can export the resulting MIDI file to 3 different targets.

# How I should stop MQP from looping the last composition?

In case you started MQP with the ```run``` command, it will playback the composition it received last time in an endless
loop (of course only if the output to MIDI interface had been activated). To stop this loop for the current composition
simply press the ```<ENTER>``` button on your keyboard multiple times.

# How I should stop MQP correctly?

If you think it is time for MQP to stop running simply press the ```q``` button on your keyboard and press ```<ENTER>``` after this.

# Utils

## Draw compositon using matplotlib

In case you start representing your notes in the fashion I do, which means ```(T,D,C,K,V)``` where ...

- **T** is time in steps
- **D** is duration in steps
- **C** is MIDI channel
- **K** is MIDI key
- **V** is MIDI velocity

... following script can be helpfull to generate plots of your score:

```python
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.collections as mc
import matplotlib.ticker as mt

def drawNotes(notes, size_x, size_y, fig_dpi):
    notesSelected = notes
    notesVis = notesSelected[notesSelected[:,1] > 0]
    notePointStart = np.column_stack((notesVis[:,0],notesVis[:,3]))
    notePointEnd = np.column_stack((notesVis[:,0]+notesVis[:,1],notesVis[:,3]))
    notePointSet = list(zip(notePointStart, notePointEnd))
    lc = mc.LineCollection(notePointSet, colors=(0, 0, 0, 1.), linewidths=2)
    fig, ax = plt.subplots(figsize=(size_x, size_y), dpi=fig_dpi)
    ax.add_collection(lc)
    ax.autoscale()
    ax.margins(0.1)
    ax.xaxis.set_major_locator(mt.MultipleLocator(spb))
    ax.yaxis.set_major_locator(mt.MultipleLocator(12))
    ax.yaxis.set_minor_locator(mt.MultipleLocator(1))
    ax.grid(which = 'major')
    ax.grid(which = 'minor')
```

## Find out all MIDI keys of a given scale

```python
import numpy as np

baseNote = 3 # D
scale = np.array([0, 2, 3, 5, 7, 8, 10]) # Minor scale

midiKeys = np.zeros(128)
for s in scale:
    keys = np.arange(baseNote + s, 128,12)
    midiKeys[keys] = 1
    
scaleKeys = np.argwhere(midiKeys).flatten()
```
