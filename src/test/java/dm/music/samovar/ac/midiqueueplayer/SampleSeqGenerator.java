package dm.music.samovar.ac.midiqueueplayer;

import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@Ignore
public class SampleSeqGenerator {

    @Test
    public void generateSample01() throws IOException {
        int headerSize = 5;
        int bpm = 123;
        int spb = 128;
        int beats = 32;
        int steps = spb * beats;
        int bodySize = steps * 16 * 128;
        byte[] seq = new byte[headerSize + bodySize];

        seq[0] = 's';
        seq[1] = 'e';
        seq[2] = 'q';
        seq[3] = (byte)(bpm & 0xFF);
        seq[4] = (byte)(spb & 0xFF);

        int startKeyId = 12 * 5;
        for(int b = 0; b < beats; b++) {
            int key = (startKeyId + b) % 128;
            for(int s = 0; s < spb; s++) {
                int t = b * spb + s;
                for(int c = 0; c < 16; c++) {
                    int cellIdx = 5 + ((16*128)*t) + (128 * c) + key;
                    seq[cellIdx] = 100;
                }
            }
        }

        Files.write(Path.of("/tmp/sequence01.seq"), seq);
    }
}
