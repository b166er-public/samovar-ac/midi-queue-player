package dm.music.samovar.ac.midiqueueplayer.common;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

public class AsciiSequenceToRawSequenceConverterTest {
    @Test
    public void testSeq004() throws IOException {
        InputStream seqStream = AsciiSequenceToRawSequenceConverterTest.class.getResourceAsStream("/seq004.seq");
        byte[] asciiSeq = IOUtils.toByteArray(seqStream);
        AsciiSequenceToRawSequenceConverter converter = new AsciiSequenceToRawSequenceConverter(
            new ByteArrayInputStream(asciiSeq)
        );
        Optional<byte[]> result = converter.convert();
        return;
    }
}
