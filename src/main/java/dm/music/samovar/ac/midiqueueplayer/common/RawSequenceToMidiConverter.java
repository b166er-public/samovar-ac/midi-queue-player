package dm.music.samovar.ac.midiqueueplayer.common;

import javax.sound.midi.*;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import static java.util.Objects.requireNonNull;

public class RawSequenceToMidiConverter {
    private final byte[] sequence;
    private final int mininumMidiResolution;

    public static final int MIDI_CHANNELS = 16;
    public static final int MIDI_KEYS = 128;
    public static final int STEP_FRAME_SIZE = MIDI_CHANNELS * MIDI_KEYS;
    public static final double MICROSECOND_PER_MINUTE = 1000000 * 60;

    private static class Header {
        public int bpm; // Beats per minute
        public int spb; // Steps per beat
    }

    public RawSequenceToMidiConverter(byte[] sequence, int mininumMidiResolution) {
        requireNonNull(sequence);
        this.mininumMidiResolution = mininumMidiResolution;
        this.sequence = sequence;
    }

    private Header getHeader() {
        boolean validPrelude = (new FileTypeDerivator(null, sequence)).hasValidSeqHeader();
        if(validPrelude) {
            Header h = new Header();
            h.bpm = ((int)sequence[3]) & 0xFF;
            h.spb = ((int)sequence[4]) & 0xFF;
            return h;
        } else {
            throw new RuntimeException("Magic number of sequence file not found!");
        }
    }

    public Sequence convert() throws InvalidMidiDataException {
        Header header = getHeader();
        int midiResolution = calculateMidiResolution(header.spb);
        Sequence seq = prepareSequence(header, midiResolution);
        int ticksPerStep = midiResolution / header.spb;
        applyArray(seq, ticksPerStep);
        return seq;
    }

    private void applyArray(Sequence seq, int ticksPerStep) throws InvalidMidiDataException {
        int numberOfFrames = (sequence.length - 5) / STEP_FRAME_SIZE;
        byte[][] keyState = new byte[MIDI_CHANNELS][MIDI_KEYS];
        Track track = seq.getTracks()[0];

        // Go through the array
        byte[] bufChn = new byte[MIDI_KEYS];
        for(int f = 0; f < numberOfFrames; f++) {
            int frameStartIdx = 5 + (STEP_FRAME_SIZE * f);
            int tickPosition = ticksPerStep * f;
            for(int c = 0; c < MIDI_CHANNELS; c++) {
                int channelStartIdx = frameStartIdx + (c * MIDI_KEYS);
                System.arraycopy(sequence,channelStartIdx,bufChn,0,MIDI_KEYS);
                boolean hasChanges = !Arrays.equals(bufChn,keyState[c]);
                if(hasChanges) {
                    for(int k = 0; k < MIDI_KEYS; k++) {
                        byte oldKeyValue = keyState[c][k];
                        byte newKeyValue = bufChn[k];
                        ShortMessage midiMessage;

                        if(oldKeyValue == newKeyValue) {
                            // Nothing to do - just optimization as it will be mostly the case
                        } else {
                            if(newKeyValue == 0) {
                                // Note off
                                midiMessage = new ShortMessage(
                                    ShortMessage.NOTE_OFF,
                                    c,
                                    k,
                                    0
                                );
                            } else if(oldKeyValue == 0) {
                                // Note on
                                midiMessage = new ShortMessage(
                                    ShortMessage.NOTE_ON,
                                    c,
                                    k,
                                    newKeyValue
                                );
                            } else {
                                // Aftertouch
                                midiMessage = new ShortMessage(
                                    ShortMessage.POLY_PRESSURE,
                                    c,
                                    k,
                                    newKeyValue
                                );
                            }
                            track.add(new MidiEvent(midiMessage, tickPosition));
                        }
                    }
                }
                // Update old state
                System.arraycopy(bufChn,0, keyState[c], 0, MIDI_KEYS);
            }
        }

        // Switch off all notes in the end
        int lastTickPosition = ticksPerStep * numberOfFrames;
        for(int c = 0; c < MIDI_CHANNELS; c++ ){
            for(int k = 0; k < MIDI_KEYS; k++) {
                if(keyState[c][k] != 0) {
                    ShortMessage noteOff = new ShortMessage(
                        ShortMessage.NOTE_OFF,
                        c,
                        k,
                        0
                    );
                    track.add(new MidiEvent(noteOff, lastTickPosition));
                }
            }
        }

        // Add message for last tick to ensure required sequence length
        String markerName = "LoopEnd";
        MetaMessage markerMsg = new MetaMessage(
            6,
            markerName.getBytes(StandardCharsets.US_ASCII),
            markerName.length()
        );
        track.add(new MidiEvent(markerMsg, lastTickPosition));
    }

    private Sequence prepareSequence(Header header, int midiResolution) throws InvalidMidiDataException {
        Sequence seq = new Sequence(Sequence.PPQ,midiResolution,1);

        // Calculate tempo
        long micSecPerBeat = (int) (MICROSECOND_PER_MINUTE / ((double) header.bpm));
        byte[] tempoData = new byte[3]; Arrays.fill(tempoData,(byte)0);
        for (int i = 0; i < 3; i++) {
            tempoData[i] = (byte) (micSecPerBeat >> ((2 - i) * 8));
        }

        // Set tempo
        MetaMessage tempoMessage = new MetaMessage(0x51, tempoData, 3);
        MidiEvent tempoEvent = new MidiEvent(tempoMessage,0);
        seq.getTracks()[0].add(tempoEvent);

        return seq;
    }

    private int calculateMidiResolution(int spb) {
        int result = this.mininumMidiResolution;
        while(result % spb != 0) {
            result++;
        }
        return result;
    }
}
