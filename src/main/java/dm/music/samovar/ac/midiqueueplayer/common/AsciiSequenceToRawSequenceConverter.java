/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package dm.music.samovar.ac.midiqueueplayer.common;

import org.tinylog.Logger;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

public class AsciiSequenceToRawSequenceConverter {

    private static class Header {
        public int bpm;
        public int spb;
        public int beats;
    }

    private final InputStream contentStream;

    private int oldT = 0; // Last time value
    private int oldD = 0; // Last duration value
    private int oldC = 0; // Last channel value
    private int oldK = 0; // Last key value
    private int oldV = 0; // Last velocity value

    public AsciiSequenceToRawSequenceConverter(InputStream contentStream) {
        requireNonNull(contentStream);
        this.contentStream = contentStream;
    }

    public Optional<byte[]> convert() {
        if(checkPrelude()) {
            Optional<Header> headerOpt = deriveHeader();
            if(headerOpt.isEmpty()) {
                Logger.error("Could not read a valid header!");
                return Optional.empty();
            } else {
                Header header = headerOpt.get();
                int steps = header.beats * header.spb;
                byte[] result = new byte[5 + (steps * 16 * 128)];
                applyHeader(header, result);
                applyCommands(result);
                return Optional.of(result);
            }
        } else {
            Logger.error("Could not indetify a valid prelude!");
            return Optional.empty();
        }
    }

    private void applyHeader(Header header, byte[] result) {
        result[0] = ((byte)0x73);     // s
        result[1] = ((byte)0x65);     // e
        result[2] = ((byte)0x71);     // q
        result[3] = (byte)((header.bpm) & 0xFF);
        result[4] = (byte)((header.spb) & 0xFF);
    }

    private void applyCommands(byte[] result) {
        boolean lastTokenReached = false;
        while(!lastTokenReached) {
            Optional<String> tokenOptional = nextToken();
            if(tokenOptional.isPresent()) {
                int newT = this.oldT;
                int newD = this.oldD;
                int newC = this.oldC;
                int newK = this.oldK;
                int newV = this.oldV;

                String token = tokenOptional.get();
                char mode = 'x';
                for(int c = 0; c < token.length(); c++) {
                    char curChar = token.charAt(c);
                    if(Character.isDigit(curChar)) {
                        switch(mode) {
                            case 't':
                            case 'T':
                                newT *= 10;
                                newT += Character.getNumericValue(curChar);
                                break;
                            case 'd':
                            case 'D':
                                newD *= 10;
                                newD += Character.getNumericValue(curChar);
                                break;
                            case 'c':
                            case 'C':
                                newC *= 10;
                                newC += Character.getNumericValue(curChar);
                                break;
                            case 'k':
                            case 'K':
                                newK *= 10;
                                newK += Character.getNumericValue(curChar);
                                break;
                            case 'v':
                            case 'V':
                                newV *= 10;
                                newV += Character.getNumericValue(curChar);
                                break;
                            default:
                                Logger.error("Found invalid token -> " + token);
                                return;
                        }
                    } else {
                        switch(curChar) {
                            case 't':
                            case 'T':
                                newT = 0;
                                mode = curChar;
                                break;
                            case 'd':
                            case 'D':
                                newD = 0;
                                mode = curChar;
                                break;
                            case 'c':
                            case 'C':
                                newC = 0;
                                mode = curChar;
                                break;
                            case 'k':
                            case 'K':
                                newK = 0;
                                mode = curChar;
                                break;
                            case 'v':
                            case 'V':
                                newV = 0;
                                mode = curChar;
                                break;
                            default:
                                Logger.error("Found invalid token -> " + token);
                                return;
                        }
                    }
                }

                applySingleCommand(newT, newD, newC, newK, newV, result);
                this.oldT = newT;
                this.oldD = newD;
                this.oldC = newC;
                this.oldK = newK;
                this.oldV = newV;
            } else {
                lastTokenReached = true;
            }
        }
    }

    private final static int OFFSET_HEADER = 5;
    private final static int OFFSET_PER_FRAME = 16 * 128;
    private final static int OFFSET_PER_CHANNEL = 128;

    private void applySingleCommand(int time, int duration, int channel, int key, int velocity, byte[] target) {
        List<Integer> idxList = new ArrayList<>(duration);
        for(int i = 0; i < duration; i++) {
            int idx = OFFSET_HEADER + ( (time+i) * OFFSET_PER_FRAME) + (channel * OFFSET_PER_CHANNEL) + key;
            if(idx < target.length) {
                idxList.add(idx);
            } else {
                Logger.error(
                    String.format(
                        "Tried to access index [%d] in a seq buffer with only [%d] elements!",
                        idx,
                        target.length
                    )
                );
            }
        }
        byte velocityByte = (byte)((velocity) & 0xFF);
        idxList.forEach( idx -> {target[idx] = velocityByte; } );
    }

    private Optional<Header> deriveHeader() {
        Optional<String> tokenOptional = nextToken();
        if(tokenOptional.isPresent()) {
            String[] headerParts = tokenOptional.get().split("-");
            if(headerParts.length == 3) {
                Header header = new Header();
                header.bpm = Integer.parseInt(headerParts[0]);
                header.spb = Integer.parseInt(headerParts[1]);
                header.beats = Integer.parseInt(headerParts[2]);
                return Optional.of(header);
            } else {
                Logger.error("Header of ASCII SEQ seems to be invalid! Syntax: <beats-per-minute>-<steps-per-beat>-<beats>");
                return Optional.empty();
            }
        } else {
            Logger.error("Could not read the ASCII SEQ header!");
            return Optional.empty();
        }
    }

    private boolean checkPrelude() {
        Optional<String> token = nextToken();
        if(token.isPresent()) {
            if("ascii-seq".equalsIgnoreCase(token.get())) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private Optional<String> nextToken() {

        try {
            StringBuilder tokenBuilder = new StringBuilder();
            int currentByte;
            boolean waitForFirstNonWhitespace = true;
            while( (currentByte = contentStream.read()) != -1) {
                char currentChar = (char) currentByte;
                if(currentChar == ' ') {
                    if(waitForFirstNonWhitespace) {
                        // do nothing as we are currently skipping whitespaces
                        // which are preceding the token we want to read
                    } else {
                        // reached terminator of token
                        break;
                    }
                } else {
                    waitForFirstNonWhitespace = false;
                    tokenBuilder.append(currentChar);
                }
            }

            String tokenString = tokenBuilder.toString().trim();
            if(tokenString.isEmpty()) {
                return Optional.empty();
            } else {
                return Optional.of(tokenString);
            }
        } catch(Exception e) {
            Logger.error("Failed to read the ASCII SEQ content!");
            e.printStackTrace();
            return Optional.empty();
        }
    }
}
