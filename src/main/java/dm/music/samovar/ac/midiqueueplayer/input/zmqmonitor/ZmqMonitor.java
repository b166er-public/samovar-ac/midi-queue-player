/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package dm.music.samovar.ac.midiqueueplayer.input.zmqmonitor;

import dm.music.samovar.ac.midiqueueplayer.common.FileTypeDerivator;
import dm.music.samovar.ac.midiqueueplayer.input.InputMqpComponent;
import dm.music.samovar.ac.midiqueueplayer.input.zmqmonitor.messages.NewMidiFileReceived;
import dm.music.samovar.ac.midiqueueplayer.input.zmqmonitor.messages.TerminateRequest;
import dm.music.samovar.ac.midiqueueplayer.input.zmqmonitor.messages.ZmpMonitorMessage;
import dm.music.samovar.ac.midiqueueplayer.loader.messages.LoadFileRequest;
import dm.music.samovar.ac.midiqueueplayer.loader.messages.MidiFileLoaderMessage;
import dm.music.samovar.ac.midiqueueplayer.input.zmqmonitor.internal.ZmqMonitorThread;
import org.tinylog.Logger;

import java.net.InetAddress;
import java.nio.file.Path;
import java.util.concurrent.BlockingQueue;

import static java.util.Objects.requireNonNull;

public class ZmqMonitor extends InputMqpComponent<ZmpMonitorMessage> {

    private final BlockingQueue<MidiFileLoaderMessage> midiLoaderQueue;
    private final InetAddress zmqIp;
    private final Integer port;

    private ZmqMonitorThread monitoringThread = null;

    public ZmqMonitor(
        BlockingQueue<ZmpMonitorMessage> requestQueue,
        Path tmpFolder,
        BlockingQueue<MidiFileLoaderMessage> midiLoaderQueue,
        InetAddress zmqIp,
        Integer zmpPort
    ) {
        super("ZMQ Monitor", requestQueue, tmpFolder);

        requireNonNull(midiLoaderQueue);
        this.midiLoaderQueue = midiLoaderQueue;
        this.zmqIp = zmqIp;
        this.port = zmpPort;
    }


    @Override
    public boolean startComponent() {
        try {
            this.monitoringThread = new ZmqMonitorThread(
                this.getComponentRequestQueue(),
                this.zmqIp,
                this.port
            );
            this.monitoringThread.start();
            Logger.info(
                String.format("Started ZMQ endpoint IP [%s] Port [%s] !",zmqIp.getHostAddress(),port.toString())
            );
        } catch(Exception e) {
            Logger.error("Could not start the monitoring thread!");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public void stopComponent() {
        try {
            this.monitoringThread.requestTermination();
            this.monitoringThread.join();
        } catch(Exception e) {
            Logger.error("Could not stop the path watch service!");
            e.printStackTrace();
        }
    }

    @Override
    public void processRequest(ZmpMonitorMessage message) {
        if(message instanceof NewMidiFileReceived) {
            NewMidiFileReceived msgTyped = (NewMidiFileReceived) message;
            try {

                FileTypeDerivator ftd = new FileTypeDerivator(
                    null,
                    msgTyped.content
                );

                if(ftd.type() != FileTypeDerivator.Type.UNKNOWN) {
                    Path tmpPath = this.storeToTmp(
                        msgTyped.content,
                        ftd.extension()
                    );
                    Logger.info(
                        String.format("Created new [%s] file in tmp folder [%s] !",
                            ftd.extension(),
                            tmpPath.toAbsolutePath().toString()
                        )
                    );
                    this.midiLoaderQueue.put(
                        new LoadFileRequest(
                            tmpPath,
                            tmpPath.getFileName().toString()
                        )
                    );
                } else {
                    Logger.error("File [%s] seems to be invalid and will be ignored!");
                }

            } catch(Exception e) {
                Logger.error("Could not put message to MIDI loader queue !");
                e.printStackTrace();
            }
        }
    }

    @Override
    public void sendTerminationRequestMessage() throws Exception {
        this.getComponentRequestQueue().put(new TerminateRequest());
    }
}