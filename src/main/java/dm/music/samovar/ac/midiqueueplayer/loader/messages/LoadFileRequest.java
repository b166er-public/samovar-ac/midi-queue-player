/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package dm.music.samovar.ac.midiqueueplayer.loader.messages;

import java.nio.file.Path;

import static java.util.Objects.requireNonNull;

public class LoadFileRequest extends MidiFileLoaderMessage{
    public final Path path;
    public final String originalFilename;

    public LoadFileRequest(Path path) {
        this(path, null);
    }

    public LoadFileRequest(Path path, String originalFilename) {
        requireNonNull(path);
        this.path = path;
        this.originalFilename = originalFilename;
    }

    public boolean isOriginalFilenameProvided() {
        return !(this.originalFilename == null);
    }
}
