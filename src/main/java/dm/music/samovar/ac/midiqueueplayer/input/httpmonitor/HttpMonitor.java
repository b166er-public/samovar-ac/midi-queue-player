/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package dm.music.samovar.ac.midiqueueplayer.input.httpmonitor;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import dm.music.samovar.ac.midiqueueplayer.common.FileTypeDerivator;
import dm.music.samovar.ac.midiqueueplayer.input.InputMqpComponent;
import dm.music.samovar.ac.midiqueueplayer.input.httpmonitor.messages.HttpMonitorMessages;
import dm.music.samovar.ac.midiqueueplayer.input.httpmonitor.messages.NewMidiFileReceived;
import dm.music.samovar.ac.midiqueueplayer.input.httpmonitor.messages.TerminateRequest;
import dm.music.samovar.ac.midiqueueplayer.loader.messages.LoadFileRequest;
import dm.music.samovar.ac.midiqueueplayer.loader.messages.MidiFileLoaderMessage;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.IOUtils;
import org.tinylog.Logger;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.file.Path;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;

import static java.util.Objects.requireNonNull;

public class HttpMonitor extends InputMqpComponent<HttpMonitorMessages> implements HttpHandler {

    private final BlockingQueue<MidiFileLoaderMessage> midiFileLoaderMessages;

    private final InetAddress ip;
    private final Integer port;

    private HttpServer httpServer = null;


    public HttpMonitor(
        BlockingQueue<HttpMonitorMessages> requestQueue,
        Path tmpFolder,
        BlockingQueue<MidiFileLoaderMessage> midiFileLoaderMessages,
        InetAddress ip,
        Integer port
    ) {
        super("HTTP Monitor", requestQueue, tmpFolder);
        requireNonNull(midiFileLoaderMessages);
        this.midiFileLoaderMessages = midiFileLoaderMessages;
        this.ip = ip;
        this.port = port;
    }

    @Override
    public void processRequest(HttpMonitorMessages message) {
        if(message instanceof NewMidiFileReceived) {
            try {
                NewMidiFileReceived msgTyped = (NewMidiFileReceived) message;

                FileTypeDerivator ftd = new FileTypeDerivator(
                    null,
                    msgTyped.content
                );

                // TO REMOVE
                // Logger.info("Content -> " + Hex.encodeHexString(msgTyped.content));

                if(ftd.type() != FileTypeDerivator.Type.UNKNOWN) {
                    Path tmpPath = this.storeToTmp(
                        msgTyped.content,
                        ftd.extension()
                    );
                    Logger.info(
                        String.format(
                            "Created new [%s] file in tmp folder [%s] !",
                            ftd.extension(),
                            tmpPath.toAbsolutePath().toString()
                        )
                    );
                    this.midiFileLoaderMessages.put(
                        new LoadFileRequest(
                            tmpPath,
                            tmpPath.getFileName().toString()
                        )
                    );
                } else {
                    Logger.error("File seems to be invalid and will be ignored!");
                }
            } catch(Exception e) {
                Logger.error("Could not send load request!");
                e.printStackTrace();
            }
        }
    }

    public boolean startComponent() {
        try {
            InetSocketAddress inetSocketAddress = prepareInetSocketAddress();
            this.httpServer = HttpServer.create(inetSocketAddress, 0);
            this.httpServer.createContext("/", this);
            this.httpServer.setExecutor(null);
            this.httpServer.start();
        } catch(Exception e) {
            Logger.error("Could not start HTTP server!");
            e.printStackTrace();
            return false;
        }

        return true;
    }

    private InetSocketAddress prepareInetSocketAddress() {

        Integer socketPort = null;
        socketPort = Objects.requireNonNullElse(this.port, 80);

        if(this.ip != null) {
            return new InetSocketAddress(this.ip, socketPort);
        } else {
            return new InetSocketAddress(socketPort);
        }
    }

    public void stopComponent() {
        httpServer.stop(0);
    }

    @Override
    public void sendTerminationRequestMessage() throws Exception {
        this.getComponentRequestQueue().put(new TerminateRequest());
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        boolean isPost = exchange.getRequestMethod().equalsIgnoreCase("POST");
        String responseMessage;
        int responseCode;

        if(isPost) {
            byte[] content = IOUtils.toByteArray(exchange.getRequestBody());

            try {
                this.getComponentRequestQueue().put(new NewMidiFileReceived(content));
                Logger.info("Received new MIDI file via HTTP!");
                responseCode = 200;
                responseMessage = "OK";
            } catch(Exception e) {
                Logger.error("Could not send the new file to the queue!");
                e.printStackTrace();
                responseCode = 400;
                responseMessage = "BAD REQUEST";
            }

        } else {
            responseMessage = "NOT FOUND";
            responseCode = 404;
        }

        exchange.sendResponseHeaders(responseCode,responseMessage.length());
        OutputStream responseBodyStream = exchange.getResponseBody();
        responseBodyStream.write(responseMessage.getBytes());
        responseBodyStream.close();
    }
}
