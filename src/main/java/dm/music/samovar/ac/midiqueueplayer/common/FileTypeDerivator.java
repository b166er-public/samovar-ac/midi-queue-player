/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package dm.music.samovar.ac.midiqueueplayer.common;

public class FileTypeDerivator {
    public enum Type {
        UNKNOWN, MIDI, SEQ_RAW, SEQ_GZ
    }

    public final static String[] extensions = {
        "broken",
        "mid",
        "midi",
        "seq",
        "seq.gz"
    };

    private final String filename;
    private final byte[] content;
    public FileTypeDerivator(String filename, byte[] content) {
        this.filename = filename;
        this.content = content;
    }

    public static FileTypeDerivator getInstance(String filename, byte[] content) {
        return new FileTypeDerivator(filename, content);
    }

    public String extension() {
        Type t = this.type();
        switch(t) {
            case UNKNOWN:
                return "broken";
            case MIDI:
                return "mid";
            case SEQ_RAW:
                return "seq";
            case SEQ_GZ:
                return "seq.gz";
            default:
                return "wtf";
        }
    }

    public Type type() {
        boolean filenameProvided = (filename != null);
        Type result = Type.UNKNOWN;

        if(hasValidMidiHeader()) {
            if(filenameProvided) {
                if(filename.toLowerCase().endsWith(".mid")) {
                    result = Type.MIDI;
                } else if(filename.toLowerCase().endsWith(".midi")) {
                    result = Type.MIDI;
                }
            } else {
                result = Type.MIDI;
            }
        } else if(hasValidSeqHeader()) {
            if(filenameProvided) {
                if(filename.toLowerCase().endsWith(".seq")) {
                    result = Type.SEQ_RAW;
                }
            } else {
                result = Type.SEQ_RAW;
            }
        } else if(hasValidGzHeader()) {
            if(filenameProvided) {
                if(filename.toLowerCase().endsWith(".seq.gz")) {
                    result = Type.SEQ_GZ;
                }
            } else {
                result = Type.SEQ_GZ;
            }
        }

        return result;
    }

    public boolean hasValidMidiHeader() {
        boolean isValid = (content[0] == ((byte)0x4D));
        isValid &= (content[1] == ((byte)0x54));
        isValid &= (content[2] == ((byte)0x68));
        isValid &= (content[3] == ((byte)0x64));
        return isValid;
    }

    public boolean hasValidSeqRawHeader() {
        boolean isValid = (content[0] == ((byte)0x73));     // s
        isValid &= (content[1] == ((byte)0x65));            // e
        isValid &= (content[2] == ((byte)0x71));            // q
        return isValid;
    }

    public boolean hasValidSeqAsciiHeader() {
        boolean isValid = (content[0] == ((byte)0x61));     // a
        isValid &= (content[1] == ((byte)0x73));            // s
        isValid &= (content[2] == ((byte)0x63));            // c
        isValid &= (content[3] == ((byte)0x69));            // i
        isValid &= (content[4] == ((byte)0x69));            // i
        isValid &= (content[5] == ((byte)0x2D));            // -
        isValid &= (content[6] == ((byte)0x73));            // s
        isValid &= (content[7] == ((byte)0x65));            // e
        isValid &= (content[8] == ((byte)0x71));            // q
        return isValid;
    }

    public boolean hasValidSeqHeader() {
        if(hasValidSeqAsciiHeader() || hasValidSeqRawHeader()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean hasValidGzHeader() {
        boolean isValid = (content[0] == ((byte)0x1F));
        isValid &= (content[1] == ((byte)0x8B));
        return isValid;
    }
}
