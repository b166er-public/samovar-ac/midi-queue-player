/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package dm.music.samovar.ac.midiqueueplayer.output.sequencectrl.internal;

import javax.sound.midi.MidiMessage;
import javax.sound.midi.Receiver;
import java.util.concurrent.atomic.AtomicBoolean;

public class MidiDeviceReceiverInterceptor implements Receiver {

    private final Receiver deviceReceiver;
    private final AtomicBoolean allowChannelModeMessages = new AtomicBoolean(true);

    public MidiDeviceReceiverInterceptor(Receiver deviceReceiver) {
        this.deviceReceiver = deviceReceiver;
    }

    public void activateChannelModeMessages() {
        this.allowChannelModeMessages.set(true);
    }

    public void deactivateChannelModeMessages() {
        this.allowChannelModeMessages.set(false);
    }

    private boolean isChannelModeMessage(MidiMessage message) {
        if( (message.getMessage()[0] & ((byte)0xF0)) == ((byte)0xB0) ) {
            return true;
        } else {
            return false;
        }
    }

    private boolean allowMessageSending(MidiMessage message) {

        if(isChannelModeMessage(message)) {
            if(this.allowChannelModeMessages.get()) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    @Override
    public void send(MidiMessage message, long timeStamp) {

        if(allowMessageSending(message)) {
            deviceReceiver.send(message, timeStamp);
        } else {
            //String msgHexString = String.valueOf(Hex.encodeHex(message.getMessage()));
            //Logger.info("Blocked MIDI OUT : " + msgHexString);
        }

    }

    @Override
    public void close() {
        deviceReceiver.close();
    }
}
