/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package dm.music.samovar.ac.midiqueueplayer.common;

import org.tinylog.Logger;

import java.util.HashMap;
import java.util.Map;

public class EnvironmentConstants {

    private static final String MQP = "MQP_";
    public static final String TMP_FOLDER_PATH = MQP + "TMP_FOLDER_PATH";
    public static final String MIN_MIDI_RESOLUTION = MQP + "MIN_MIDI_RESOLUTION";

    public static class Input {
        private static final String INPUT = MQP + "INPUT_";

        public static class FileSystem {
            private static final String FILE_SYSTEM = INPUT + "FS_";
            public static final String ACTIVATED = FILE_SYSTEM + "ACTIVATED";
            public static final String FOLDER_PATH = FILE_SYSTEM + "FOLDER_PATH";
        }

        public static class HTTP {
            private static final String HTTP = INPUT + "HTTP_";
            public static final String ACTIVATED = HTTP + "ACTIVATED";
            public static final String IP = HTTP + "IP";
            public static final String PORT = HTTP + "PORT";
        }

        public static class ZMQ {
            private static final String ZMQ = INPUT + "ZMQ_";
            public static final String ACTIVATED = ZMQ + "ACTIVATED";
            public static final String IP = ZMQ + "IP";
            public static final String PORT = ZMQ + "PORT";
        }
    }

    public static class Output {
        private static final String OUTPUT = MQP + "OUTPUT_";

        public static class FileSystem {
            private static final String FILE_SYSTEM = OUTPUT + "FS_";
            public static final String ACTIVATED = FILE_SYSTEM + "ACTIVATED";
            public static final String FOLDER_PATH = FILE_SYSTEM + "FOLDER_PATH";
        }

        public static class MIDI {
            private static final String MIDI = OUTPUT + "MIDI_";
            public static final String ACTIVATED = MIDI + "ACTIVATED";
            public static final String DEVICE_ID= MIDI + "DEVICE_ID";
        }

        public static class ZMQ {
            private static final String ZMQ = OUTPUT + "ZMQ_";
            public static final String ACTIVATED = ZMQ + "ACTIVATED";
            public static final String IP = ZMQ + "IP";
            public static final String PORT = ZMQ + "PORT";
        }
    }

    private static StringBuilder addHelpLine(StringBuilder builder, String envVar, String helpMsg) {
        builder.append(
            String.format("[%s] = %s\n", envVar, helpMsg)
        );
        return builder;
    }

    public static void printInfo() {

        StringBuilder sb = new StringBuilder();
        sb.append("<< Generic configuration >>\n");
        addHelpLine(sb,TMP_FOLDER_PATH,"Location where intermediate files should be stored");
        addHelpLine(sb,MIN_MIDI_RESOLUTION,"The minimal MIDI file resolution (ticks per beat).");
        sb.append("\n");

        sb.append("<< Input Layer - File System >>\n");
        addHelpLine(sb,Input.FileSystem.ACTIVATED,"Should monitoring of file-system be activated?");
        addHelpLine(sb,Input.FileSystem.FOLDER_PATH,"Which folder should be monitored for playback material?");
        sb.append("\n");

        sb.append("<< Input Layer - HTTP >>\n");
        addHelpLine(sb,Input.HTTP.ACTIVATED,"Should monitoring of HTTP endpoint be activated?");
        addHelpLine(sb,Input.HTTP.IP,"IP address which the HTTP endpoint should be bound to.");
        addHelpLine(sb,Input.HTTP.PORT,"Port which the HTTP endpoint should be bound to.");
        sb.append("\n");

        sb.append("<< Input Layer - ZMQ >>\n");
        addHelpLine(sb,Input.ZMQ.ACTIVATED,"Should monitoring of ZMQ endpoint be activated?");
        addHelpLine(sb,Input.ZMQ.IP,"IP address which the ZMQ endpoint should be bound to.");
        addHelpLine(sb,Input.ZMQ.PORT,"Port which the ZMQ endpoint should be bound to.");
        sb.append("\n");

        sb.append("<< Output Layer - File System >>\n");
        addHelpLine(sb,Output.FileSystem.ACTIVATED,"Should export to file-system be activated?");
        addHelpLine(sb,Output.FileSystem.FOLDER_PATH,"Which folder should be used for export of received playback material?");
        sb.append("\n");
        sb.append("<< Output Layer - MIDI >>\n");
        addHelpLine(sb,Output.MIDI.ACTIVATED,"Should export to MIDI OUT be activated?");
        addHelpLine(sb,Output.MIDI.DEVICE_ID,"Id of the MIDI device which will be used for playback.");
        sb.append("\n");
        sb.append("<< Output Layer - ZMQ >>\n");
        addHelpLine(sb,Output.ZMQ.ACTIVATED,"Should export to ZMQ endpoint be activated?");
        addHelpLine(sb,Output.ZMQ.IP,"IP address where the ZMQ endpoint can be reached.");
        addHelpLine(sb,Output.ZMQ.PORT,"Port where the ZMQ endpoint can be reached.");
        sb.append("\n");
        sb.append("\n");
        Logger.info("Following environment variables are used by MQP: \n\n" + sb.toString());
    }

    public static Map<String, String> getRelevantVariables() {
        Map<String, String> envs = System.getenv();
        Map<String, String> resultEnvs = new HashMap<>();

        for(String k: envs.keySet()) {
            if(k.startsWith(EnvironmentConstants.MQP)) {
                resultEnvs.put(k,envs.get(k));
            }
        }

        return resultEnvs;
    }

    public static void printRelevantVariables() {
        Map<String, String> relevantEnvs = getRelevantVariables();

        if(relevantEnvs.isEmpty()) {
            Logger.warn("No relevant environment variable had been found!");
        } else {
            for(String k: relevantEnvs.keySet()) {
                Logger.info(String.format("Environment variable [%s] -> [%s]",k, relevantEnvs.getOrDefault(k,"UNDEFINED")));
            }
        }
    }

}
