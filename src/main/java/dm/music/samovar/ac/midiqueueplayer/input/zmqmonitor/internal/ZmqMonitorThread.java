/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package dm.music.samovar.ac.midiqueueplayer.input.zmqmonitor.internal;

import dm.music.samovar.ac.midiqueueplayer.input.zmqmonitor.messages.NewMidiFileReceived;
import dm.music.samovar.ac.midiqueueplayer.input.zmqmonitor.messages.ZmpMonitorMessage;

import org.tinylog.Logger;
import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

import java.net.InetAddress;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.util.Objects.requireNonNull;

public class ZmqMonitorThread extends Thread {

    private final BlockingQueue<ZmpMonitorMessage> zmpMonitorQueue;
    private final InetAddress ip;
    private final Integer port;

    private final AtomicBoolean terminationRequested = new AtomicBoolean(false);

    private ZContext zmqContext = null;
    private ZMQ.Socket zmqSocket = null;

    public ZmqMonitorThread(
        BlockingQueue<ZmpMonitorMessage> zmpMonitorQueue,
        InetAddress ip,
        Integer port
    ) {
        super("ZMQ Monitor (watch thread)");

        requireNonNull(zmpMonitorQueue);

        this.zmpMonitorQueue = zmpMonitorQueue;
        this.ip = ip;
        this.port = port;
    }

    public boolean startMonitoring() {
        try {
            zmqContext = new ZContext();
            zmqSocket = zmqContext.createSocket(SocketType.REP);

            zmqSocket.bind(
                buildConnectionString()
            );

            zmqSocket.setReceiveTimeOut(1000);
        } catch(Exception e) {
            Logger.error("Could not open the ZMQ Socket!");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private String buildConnectionString() {
        StringBuilder sb = new StringBuilder();
        sb.append("tcp://");

        if(this.ip == null) {
            sb.append("*");
        } else {
            sb.append(ip.getHostAddress());
        }

        sb.append(":");

        if(this.port == null) {
            sb.append("5555");
        } else {
            sb.append(port);
        }

        return sb.toString();
    }

    public void stopMonitoring() {
        try {
            zmqSocket.close();
            zmqContext.close();
        } catch(Exception e) {
            Logger.error("Could not stop the ZMQ socket!");
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        this.startMonitoring();

        try {
            while(!this.terminationRequested.get()) {
                byte[] newContent = zmqSocket.recv();
                if(newContent != null) {
                    zmpMonitorQueue.put(new NewMidiFileReceived(newContent));
                    zmqSocket.send("OK");
                }
            }
        } catch(Exception e) {
            Logger.error("Could not poll from watch service!");
            e.printStackTrace();
        }

        this.stopMonitoring();
    }

    public void requestTermination() {
        this.terminationRequested.set(true);
    }
}

