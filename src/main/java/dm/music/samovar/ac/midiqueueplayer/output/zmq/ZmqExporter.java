/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package dm.music.samovar.ac.midiqueueplayer.output.zmq;

import dm.music.samovar.ac.midiqueueplayer.output.OutputMqpComponent;
import dm.music.samovar.ac.midiqueueplayer.output.zmq.messages.SendMidiFile;
import dm.music.samovar.ac.midiqueueplayer.output.zmq.messages.TerminateRequest;
import dm.music.samovar.ac.midiqueueplayer.output.zmq.messages.ZmqExporterMessages;
import org.tinylog.Logger;
import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;

import javax.sound.midi.MidiSystem;
import java.io.ByteArrayOutputStream;
import java.net.InetAddress;
import java.util.concurrent.BlockingQueue;

import static java.util.Objects.requireNonNull;

public class ZmqExporter extends OutputMqpComponent<ZmqExporterMessages> {

    private final InetAddress ip;
    private final Integer port;


    public ZmqExporter(BlockingQueue<ZmqExporterMessages> requestQueue, InetAddress ip, Integer port) {
        super("ZMQ Exporter", requestQueue);
        requireNonNull(ip);
        requireNonNull(port);
        this.ip = ip;
        this.port = port;
    }

    @Override
    public void processRequest(ZmqExporterMessages message) {
        
        if(message instanceof SendMidiFile) {
            SendMidiFile msgTyped = (SendMidiFile) message;

            try {
                ByteArrayOutputStream midiFileInMemory = new ByteArrayOutputStream();
                MidiSystem.write(
                    msgTyped.content,
                    1,
                    midiFileInMemory
                );

                ZContext zmqContext = new ZContext();
                ZMQ.Socket zmqSocket = zmqContext.createSocket(SocketType.REQ);
                zmqSocket.send(midiFileInMemory.toByteArray());

                zmqSocket.close();
                zmqContext.close();
            } catch(Exception e) {
                Logger.error("Could not send the MIDI file to ZMQ Endpoint!");
                e.printStackTrace();
            }
        }
    }

    @Override
    public void sendTerminationRequestMessage() throws Exception {
        this.getComponentRequestQueue().put(new TerminateRequest());
    }
}
