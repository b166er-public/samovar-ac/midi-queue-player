/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package dm.music.samovar.ac.midiqueueplayer.commands;

import dm.music.samovar.ac.midiqueueplayer.common.EnvironmentConstants;
import org.tinylog.Logger;
import picocli.CommandLine;

import java.util.concurrent.Callable;

@CommandLine.Command(
    name = "env",
    description = "List all mqp relevant environment variables",
    mixinStandardHelpOptions = true
)
public class EnvCommand implements Callable<Integer> {
    @Override
    public Integer call() throws Exception {
        EnvironmentConstants.printInfo();

        Logger.info("Following environment variables I see:");
        EnvironmentConstants.printRelevantVariables();

        return 0;
    }
}
