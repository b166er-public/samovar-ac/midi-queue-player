/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package dm.music.samovar.ac.midiqueueplayer.input;

import dm.music.samovar.ac.midiqueueplayer.common.EnvironmentConstants;
import dm.music.samovar.ac.midiqueueplayer.input.fsmonitor.FileSystemMonitor;
import dm.music.samovar.ac.midiqueueplayer.input.fsmonitor.messages.FileSystemMonitorMessage;
import dm.music.samovar.ac.midiqueueplayer.input.httpmonitor.HttpMonitor;
import dm.music.samovar.ac.midiqueueplayer.input.httpmonitor.messages.HttpMonitorMessages;
import dm.music.samovar.ac.midiqueueplayer.input.zmqmonitor.ZmqMonitor;
import dm.music.samovar.ac.midiqueueplayer.input.zmqmonitor.messages.ZmpMonitorMessage;
import dm.music.samovar.ac.midiqueueplayer.loader.messages.MidiFileLoaderMessage;
import org.tinylog.Logger;

import java.io.IOException;
import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static java.util.Objects.requireNonNull;

public class InputMqpManager {

    private final BlockingQueue<MidiFileLoaderMessage> midiFileLoaderQueue;
    private final Map<String,String> environment;

    private final BlockingQueue<FileSystemMonitorMessage> fsQueue = new LinkedBlockingQueue();
    private final BlockingQueue<ZmpMonitorMessage> zmqQueue = new LinkedBlockingQueue();
    private final BlockingQueue<HttpMonitorMessages> httpQueue = new LinkedBlockingQueue();

    private FileSystemMonitor fs = null;
    private ZmqMonitor zmq = null;
    private HttpMonitor http = null;


    public InputMqpManager(
        BlockingQueue<MidiFileLoaderMessage> midiFileLoaderQueue,
        Map<String, String> environment
    ) {
        requireNonNull(midiFileLoaderQueue);
        requireNonNull(environment);
        this.midiFileLoaderQueue = midiFileLoaderQueue;
        this.environment = environment;
    }

    private Path prepareTmpFolder() throws IOException {
        String envFolderPath = environment.get( EnvironmentConstants.TMP_FOLDER_PATH );

        Path tmpFolderPath = null;
        if(envFolderPath != null) {
            tmpFolderPath = Path.of(envFolderPath).toAbsolutePath();
            tmpFolderPath.toFile().mkdirs();
        } else {
            tmpFolderPath = Files.createTempDirectory("mqp-");
        }

        return tmpFolderPath;
    }

    public void start() throws Exception {

        Path tmpFolder = prepareTmpFolder();

        Boolean fsActivated = Boolean.parseBoolean(
            environment.getOrDefault(
                EnvironmentConstants.Input.FileSystem.ACTIVATED,
                "false"
            ).toLowerCase()
        );

        Boolean zmqActivated = Boolean.parseBoolean(
            environment.getOrDefault(
                EnvironmentConstants.Input.ZMQ.ACTIVATED,
                "false"
            ).toLowerCase()
        );

        Boolean httpActivated = Boolean.parseBoolean(
            environment.getOrDefault(
                EnvironmentConstants.Input.HTTP.ACTIVATED,
                "false"
            ).toLowerCase()
        );

        // File System Monitor
        if(fsActivated) {
            Path watchFolder = Path.of(
                environment.get(EnvironmentConstants.Input.FileSystem.FOLDER_PATH)
            );

            this.fs = new FileSystemMonitor(
                fsQueue,
                tmpFolder,
                midiFileLoaderQueue,
                watchFolder
            );

            this.fs.start();
        } else {
            Logger.info("File system monitor is not activated!");
        }

        // ZMP Monitor
        if(zmqActivated) {
            InetAddress ip = InetAddress.getByName(
                environment.get(EnvironmentConstants.Input.ZMQ.IP)
            );
            Integer port = Integer.parseInt(
                environment.get(EnvironmentConstants.Input.ZMQ.PORT)
            );

            this.zmq = new ZmqMonitor(
                zmqQueue,
                tmpFolder,
                midiFileLoaderQueue,
                ip,
                port
            );

            this.zmq.start();
        } else {
            Logger.info("ZMQ monitor is not activated!");
        }

        // HTTP Monitor
        if(httpActivated) {
            InetAddress ip = InetAddress.getByName(
                environment.get(EnvironmentConstants.Input.HTTP.IP)
            );
            Integer port = Integer.parseInt(
                environment.get(EnvironmentConstants.Input.HTTP.PORT)
            );

            this.http = new HttpMonitor(
                httpQueue,
                tmpFolder,
                midiFileLoaderQueue,
                ip,
                port
            );

            this.http.start();
        }else {
            Logger.info("HTTP monitor is not activated!");
        }
    }

    public void requestTermination() throws Exception {

        if(this.fs != null) { this.fs.sendTerminationRequestMessage(); }
        if(this.zmq != null) { this.zmq.sendTerminationRequestMessage(); }
        if(this.http != null) { this.http.sendTerminationRequestMessage(); }

    }

    public void join() throws Exception {
        if(this.fs != null) { this.fs.join(); }
        if(this.zmq != null) { this.zmq.join(); }
        if(this.http != null) { this.http.join(); }
    }
}
