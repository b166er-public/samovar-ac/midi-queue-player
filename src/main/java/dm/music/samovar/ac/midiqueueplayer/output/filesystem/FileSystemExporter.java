/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package dm.music.samovar.ac.midiqueueplayer.output.filesystem;

import dm.music.samovar.ac.midiqueueplayer.output.OutputMqpComponent;
import dm.music.samovar.ac.midiqueueplayer.output.filesystem.messages.ExportMidiFile;
import dm.music.samovar.ac.midiqueueplayer.output.filesystem.messages.FileSystemExporerMessages;
import dm.music.samovar.ac.midiqueueplayer.output.filesystem.messages.TerminateRequest;
import org.tinylog.Logger;

import javax.sound.midi.MidiSystem;
import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.BlockingQueue;

import static java.util.Objects.requireNonNull;

public class FileSystemExporter extends OutputMqpComponent<FileSystemExporerMessages> {

    private final Path targetPath;

    public FileSystemExporter(BlockingQueue<FileSystemExporerMessages> requestQueue, Path targetPath) {
        super("File System Exporter", requestQueue);
        requireNonNull(targetPath);
        this.targetPath = targetPath;
    }

    @Override
    public void processRequest(FileSystemExporerMessages message) {
        if(message instanceof ExportMidiFile) {
            ExportMidiFile msgTyped = (ExportMidiFile) message;
            String filenameNoExt = removeExtensionFromFilename(msgTyped.originalFileName);
            Path targetFilePath = targetPath.toAbsolutePath().resolve(filenameNoExt + ".mid");
            try {
                MidiSystem.write(
                    msgTyped.content,
                    1,
                    targetFilePath.toFile()
                );
                Logger.info(String.format("Wrote new MIDI file to filesystem [%s]",targetFilePath.toString()));
            } catch (IOException e) {
                Logger.error(String.format("Could not write MIDI file to [%s]",targetFilePath.toString()));
                e.printStackTrace();
            }
        }

    }

    private String removeExtensionFromFilename(String originalFileName) {
        return originalFileName.split("\\.")[0];
    }

    @Override
    public void sendTerminationRequestMessage() throws Exception {
        this.getComponentRequestQueue().put(new TerminateRequest());
    }
}
