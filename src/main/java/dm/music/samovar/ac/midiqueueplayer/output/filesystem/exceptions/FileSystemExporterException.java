package dm.music.samovar.ac.midiqueueplayer.output.filesystem.exceptions;

public class FileSystemExporterException extends RuntimeException {
    public FileSystemExporterException() {
    }

    public FileSystemExporterException(String message) {
        super(message);
    }

    public FileSystemExporterException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileSystemExporterException(Throwable cause) {
        super(cause);
    }

    public FileSystemExporterException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
