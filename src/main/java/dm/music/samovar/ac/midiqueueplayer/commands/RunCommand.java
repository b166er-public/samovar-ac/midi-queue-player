/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package dm.music.samovar.ac.midiqueueplayer.commands;

import dm.music.samovar.ac.midiqueueplayer.common.EnvironmentConstants;
import dm.music.samovar.ac.midiqueueplayer.input.InputMqpManager;
import dm.music.samovar.ac.midiqueueplayer.loader.MidiFileLoader;
import dm.music.samovar.ac.midiqueueplayer.loader.messages.MidiFileLoaderMessage;
import dm.music.samovar.ac.midiqueueplayer.output.sequencectrl.messages.MidiSequencerControllerMessages;
import org.tinylog.Logger;
import picocli.CommandLine;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;

@CommandLine.Command(
    name = "run",
    description = "Will run the midi-queue-player",
    mixinStandardHelpOptions = true
)
public class RunCommand implements Callable<Integer> {


    @Override
    public Integer call() throws Exception {
        Logger.info("Command [run] had been selected!");

        Map<String, String> sysEnvs = EnvironmentConstants.getRelevantVariables();

        // Print all relevant environment variables
        EnvironmentConstants.printRelevantVariables();

        // Build up the queues
        BlockingQueue<MidiFileLoaderMessage> midiFileLoaderQueue = new LinkedBlockingQueue();
        BlockingQueue<MidiSequencerControllerMessages> midiSequencerControllerQueue = new LinkedBlockingQueue();

        // Create components
        InputMqpManager inputManager = new InputMqpManager(midiFileLoaderQueue, sysEnvs);

        MidiFileLoader midiFileLoader = new MidiFileLoader(
            midiFileLoaderQueue,
            sysEnvs
        );

        // Start components
        inputManager.start();
        midiFileLoader.start();

        // Receive commands via CLI
        BufferedReader cliReader = new BufferedReader(
            new InputStreamReader(System.in)
        );

        boolean terminationRequested = false;
        while(!terminationRequested) {
            String command = cliReader.readLine();

            if(command.isEmpty()) {
                // Stop looping last sequence
                midiFileLoader.requestStopLooping();
                Logger.info("User interrupted looping using <ENTER>!");
            }
            else if(command.trim().startsWith("e") || command.trim().startsWith("q")) {
                Logger.info("MQP received a termination request from the user!");
                terminationRequested = true;
            }
        }

        // Termination
        Logger.info("Termination procedure started!");
        inputManager.requestTermination();
        midiFileLoader.sendTerminationRequestMessage();

        inputManager.join();
        midiFileLoader.join();
        Logger.info("Termination procedure completed!");

        return 0;
    }
}
