/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package dm.music.samovar.ac.midiqueueplayer.loader.exceptions;

public class CorruptedMidiFileException extends MidiFileLoaderException {
    public CorruptedMidiFileException() {
    }

    public CorruptedMidiFileException(String message) {
        super(message);
    }

    public CorruptedMidiFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public CorruptedMidiFileException(Throwable cause) {
        super(cause);
    }

    public CorruptedMidiFileException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
