/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package dm.music.samovar.ac.midiqueueplayer.input;

import dm.music.samovar.ac.midiqueueplayer.common.MqpComponent;
import dm.music.samovar.ac.midiqueueplayer.common.MqpMessage;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;

import static java.util.Objects.requireNonNull;

public abstract class InputMqpComponent<M extends MqpMessage> extends MqpComponent<M> {

    private final Path tmpFolder;

    public InputMqpComponent(String name, BlockingQueue<M> requestQueue, Path tmpFolder) {
        super(name, requestQueue);
        requireNonNull(tmpFolder);
        this.tmpFolder = tmpFolder;
    }

    protected Path storeToTmp(byte[] content, String extension) {

        requireNonNull(content);
        requireNonNull(extension);

        LocalDateTime now = LocalDateTime.now();
        UUID uuid = UUID.randomUUID();
        DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss-SS");
        StringBuilder filenameBuilder = new StringBuilder();
        filenameBuilder.append(dtFormatter.format(now));
        filenameBuilder.append("-");
        filenameBuilder.append(uuid.toString());
        filenameBuilder.append("." + extension);

        String filename = filenameBuilder.toString();
        Path tmpFilePath = this.tmpFolder.toAbsolutePath().resolve(filename);


        try (FileOutputStream stream = new FileOutputStream(tmpFilePath.toFile())) {
            stream.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return tmpFilePath;
    }
}
