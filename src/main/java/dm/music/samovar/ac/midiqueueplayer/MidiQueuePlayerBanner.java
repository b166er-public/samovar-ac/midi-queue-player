/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package dm.music.samovar.ac.midiqueueplayer;

public class MidiQueuePlayerBanner {
    public static void printBanner() {
        // Generated at https://devops.datenkollektiv.de/banner.txt/index.html
        // Text : MQP
        // Style : block

        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        sb.append("_|      _|     _|_|       _|_|_|\n");
        sb.append("_|_|  _|_|   _|    _|     _|    _|\n");
        sb.append("_|  _|  _|   _|  _|_|     _|_|_|\n");
        sb.append("_|      _|   _|    _|     _|\n");
        sb.append("_|      _|     _|_|  _|   _|\n");
        sb.append("\n");
        sb.append("Written by Daniil Moerman\n");
        sb.append("Version: 1.0.0");
        sb.append("\n");
        sb.append("\n");


        System.out.println(sb.toString());


    }
}
