/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package dm.music.samovar.ac.midiqueueplayer.output;

import dm.music.samovar.ac.midiqueueplayer.common.EnvironmentConstants;
import dm.music.samovar.ac.midiqueueplayer.output.filesystem.FileSystemExporter;
import dm.music.samovar.ac.midiqueueplayer.output.filesystem.messages.ExportMidiFile;
import dm.music.samovar.ac.midiqueueplayer.output.filesystem.messages.FileSystemExporerMessages;
import dm.music.samovar.ac.midiqueueplayer.output.sequencectrl.MidiSequencerController;
import dm.music.samovar.ac.midiqueueplayer.output.sequencectrl.messages.MidiSequencerControllerMessages;
import dm.music.samovar.ac.midiqueueplayer.output.sequencectrl.messages.PlaySequenceRequest;
import dm.music.samovar.ac.midiqueueplayer.output.sequencectrl.messages.StopLoopRequest;
import dm.music.samovar.ac.midiqueueplayer.output.zmq.ZmqExporter;
import dm.music.samovar.ac.midiqueueplayer.output.zmq.messages.SendMidiFile;
import dm.music.samovar.ac.midiqueueplayer.output.zmq.messages.ZmqExporterMessages;
import org.tinylog.Logger;

import javax.sound.midi.Sequence;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static java.util.Objects.requireNonNull;

public class OutputMqpManager {

    private final Map<String,String> environment;

    private final BlockingQueue<FileSystemExporerMessages> fsQueue = new LinkedBlockingQueue();
    private final BlockingQueue<MidiSequencerControllerMessages> seqQueue = new LinkedBlockingQueue();
    private final BlockingQueue<ZmqExporterMessages> zmqQueue = new LinkedBlockingQueue();

    private FileSystemExporter fsExporter = null;
    private MidiSequencerController seqExporter = null;
    private ZmqExporter zmqExporter = null;


    public OutputMqpManager(Map<String, String> environment) {
        requireNonNull(environment);
        this.environment = environment;
    }

    public void start() throws UnknownHostException {

        // Activation check
        Boolean fsActivated = Boolean.parseBoolean(
            environment.getOrDefault(
                EnvironmentConstants.Output.FileSystem.ACTIVATED,
                "false"
            ).toLowerCase()
        );

        Boolean seqActivated = Boolean.parseBoolean(
            environment.getOrDefault(
                EnvironmentConstants.Output.MIDI.ACTIVATED,
                "false"
            ).toLowerCase()
        );

        Boolean zmqActivated = Boolean.parseBoolean(
            environment.getOrDefault(
                EnvironmentConstants.Output.ZMQ.ACTIVATED,
                "false"
            ).toLowerCase()
        );

        // File System Exporter
        if(fsActivated) {
            Path targetFolder = Path.of(
                environment.get(EnvironmentConstants.Output.FileSystem.FOLDER_PATH)
            );

            this.fsExporter = new FileSystemExporter(
                fsQueue,
                targetFolder
            );

            this.fsExporter.start();
        } else {
            Logger.info("File system exporter is not activated!");
        }

        // MIDI Exporter
        if(seqActivated) {
            String midiDeviceId =
                environment.get(EnvironmentConstants.Output.MIDI.DEVICE_ID);

            this.seqExporter = new MidiSequencerController(
                seqQueue,
                midiDeviceId
            );

            this.seqExporter.start();
        }

        // ZMP Exporter
        if(zmqActivated) {
            InetAddress ip = InetAddress.getByName(
                environment.get(EnvironmentConstants.Output.ZMQ.IP)
            );
            Integer port = Integer.parseInt(
                environment.get(EnvironmentConstants.Output.ZMQ.PORT)
            );

            this.zmqExporter = new ZmqExporter(
                zmqQueue,
                ip,
                port
            );

            this.zmqExporter.start();
        } else {
            Logger.info("ZMQ exporter is not activated!");
        }

    }

    public void requestTermination() throws Exception {
        if(this.fsExporter != null) { this.fsExporter.sendTerminationRequestMessage(); }
        if(this.seqExporter != null) { this.seqExporter.sendTerminationRequestMessage(); }
        if(this.zmqExporter != null) { this.zmqExporter.sendTerminationRequestMessage(); }
    }

    public void join() throws InterruptedException {
        if(this.fsExporter != null) { this.fsExporter.join(); }
        if(this.seqExporter != null) { this.seqExporter.join(); }
        if(this.zmqExporter != null) { this.zmqExporter.join(); }
    }

    public void processSequence(Sequence content, String originalFilename) throws InterruptedException {

        if(originalFilename == null) {
            originalFilename = "!!! UNDEFINED FILENAME !!!";
        }

        if(this.fsExporter != null) {
            this.fsQueue.put(new ExportMidiFile(content, originalFilename));
        }

        if(this.seqExporter != null) {
            this.seqQueue.put(new PlaySequenceRequest(content, originalFilename));
        }

        if(this.zmqExporter != null) {
            this.zmqQueue.put(new SendMidiFile(content, originalFilename));
        }
    }

    public void requestStopLooping() throws InterruptedException {
        if(this.seqExporter != null) {
            this.seqQueue.put(new StopLoopRequest());
        }
    }
}
