/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package dm.music.samovar.ac.midiqueueplayer.common;

import org.tinylog.Logger;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.util.Objects.requireNonNull;

public abstract class MqpComponent<M extends MqpMessage> extends Thread {

    private final BlockingQueue<M> requestQueue;
    private final AtomicBoolean terminationRequested = new AtomicBoolean(false);

    public MqpComponent(
        String name,
        BlockingQueue<M> requestQueue
    ) {
        super(name);
        requireNonNull(name);
        requireNonNull(requestQueue);
        this.requestQueue = requestQueue;
    }

    protected BlockingQueue<M> getComponentRequestQueue() {
        return this.requestQueue;
    }

    final public void sendMessage(M message) throws InterruptedException {
        requestQueue.put(message);
    }

    @Override
    final public void run() {
        // Start
        if(!startComponent()) {
            String errorMsg = String.format("Component [%s] failed to start!", this.getName());
            Logger.error(errorMsg);
            throw new RuntimeException(errorMsg);
        } else {
            Logger.info(String.format("Component [%s] started!", this.getName()));
        }

        // Work
        while(!isTerminationRequested()) {
            try {
                M message = requestQueue.take();

                if(message.isTerminal()) {
                    this.requestTermination();
                } else {
                    this.processRequest(message);
                }
            } catch (InterruptedException e) {
                Logger.error(String.format("Component [%s] had been interrupted!", this.getName()));
                e.printStackTrace();
                break;
            }
        }

        // Stop
        Logger.info(String.format("Component [%s] received termination request!",this.getName()));
        stopComponent();
        Logger.info(String.format("Component [%s] terminated successfully!",this.getName()));
    }

    public boolean startComponent() { return true; }
    public void stopComponent() {}
    public boolean isTerminationRequested() {
        return this.terminationRequested.get();
    }

    protected void requestTermination() {
        this.terminationRequested.set(true);
    }

    public abstract void processRequest(M message);

    public abstract void sendTerminationRequestMessage() throws Exception;
}
