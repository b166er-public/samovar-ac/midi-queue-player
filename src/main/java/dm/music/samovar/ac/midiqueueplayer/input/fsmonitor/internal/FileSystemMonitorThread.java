/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package dm.music.samovar.ac.midiqueueplayer.input.fsmonitor.internal;

import dm.music.samovar.ac.midiqueueplayer.common.FileTypeDerivator;
import dm.music.samovar.ac.midiqueueplayer.input.fsmonitor.messages.FileSystemMonitorMessage;
import dm.music.samovar.ac.midiqueueplayer.input.fsmonitor.messages.NewMidiFileDetected;
import org.tinylog.Logger;

import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.util.Objects.requireNonNull;

public class FileSystemMonitorThread extends Thread {

    private final Path watchFolder;
    private final BlockingQueue<FileSystemMonitorMessage> fileSystemMonitorQueue;

    private final AtomicBoolean terminationRequested = new AtomicBoolean(false);

    private WatchKey watchKey = null;
    private Path watchPath = null;
    private WatchService watchService = null;

    public FileSystemMonitorThread(
        Path watchFolder,
        BlockingQueue<FileSystemMonitorMessage> fileSystemMonitorQueue
    ) {
        super("File System Monitor (watch thread)");
        requireNonNull(watchFolder);
        requireNonNull(fileSystemMonitorQueue);
        this.watchFolder = watchFolder;
        this.fileSystemMonitorQueue = fileSystemMonitorQueue;
    }

    public boolean startMonitoring() {
        try {
            watchService = FileSystems.getDefault().newWatchService();
            watchPath = watchFolder;
            watchKey = watchPath.register(watchService, StandardWatchEventKinds.ENTRY_CREATE);
        } catch(Exception e) {
            Logger.error("Could not register a watch on source-folder!");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public void stopMonitoring() {
        try {
            watchKey.cancel();
            watchService.close();
        } catch(Exception e) {
            Logger.error("Could not stop the path watch service!");
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        this.startMonitoring();

        try {
            while(!this.terminationRequested.get()) {
                List<Path> newPathList = this.pollWatchService();
                for(Path p : newPathList) {
                    Logger.info(
                        String.format("New file detected [%s]",p.toAbsolutePath().toString())
                    );
                    fileSystemMonitorQueue.put(new NewMidiFileDetected(p));
                }
            }
        } catch(Exception e) {
            Logger.error("Could not poll from watch service!");
            e.printStackTrace();
        }

        this.stopMonitoring();
    }

    private List<Path> pollWatchService() throws InterruptedException {
        List<Path> result = new ArrayList<>();

        WatchKey newKey = watchService.poll(100, TimeUnit.MILLISECONDS);

        if(newKey != null) {
            boolean fileModifiedEventReceived = false;
            boolean fileModified = false;

            for(WatchEvent<?> event : newKey.pollEvents())
            {
                if(event.kind() == StandardWatchEventKinds.ENTRY_CREATE)
                {
                    Path newFilePath = watchPath.resolve((Path) event.context());
                    String newFilePathAbsolute = newFilePath.toAbsolutePath().toFile().getAbsolutePath();

                    // Check if this is a relevant file (by extension)
                    for(String ext : FileTypeDerivator.extensions) {
                        if(newFilePathAbsolute.endsWith("."+ext)) {
                            result.add(newFilePath.toAbsolutePath());
                            break;
                        }
                    }
                }
            }
            newKey.reset();
        }


        return result;
    }

    public void requestTermination() {
        this.terminationRequested.set(true);
    }
}
