/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package dm.music.samovar.ac.midiqueueplayer.output;

import dm.music.samovar.ac.midiqueueplayer.common.MqpComponent;
import dm.music.samovar.ac.midiqueueplayer.common.MqpMessage;

import java.util.concurrent.BlockingQueue;

public abstract class OutputMqpComponent<M extends MqpMessage> extends MqpComponent<M> {
    public OutputMqpComponent(String name, BlockingQueue<M> requestQueue) {
        super(name, requestQueue);
    }
}
