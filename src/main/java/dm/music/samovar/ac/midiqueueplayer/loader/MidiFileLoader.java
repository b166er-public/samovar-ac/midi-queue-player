/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package dm.music.samovar.ac.midiqueueplayer.loader;

import dm.music.samovar.ac.midiqueueplayer.common.*;
import dm.music.samovar.ac.midiqueueplayer.loader.messages.LoadFileRequest;
import dm.music.samovar.ac.midiqueueplayer.loader.messages.MidiFileLoaderMessage;
import dm.music.samovar.ac.midiqueueplayer.loader.messages.TerminateRequest;
import dm.music.samovar.ac.midiqueueplayer.output.OutputMqpManager;
import org.apache.commons.io.FileUtils;
import org.tinylog.Logger;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.zip.GZIPInputStream;

import static java.util.Objects.requireNonNull;

public class MidiFileLoader extends MqpComponent<MidiFileLoaderMessage> {

    private final Map<String,String> environment;

    private int minimalMidiResolution = -1;
    private OutputMqpManager outputManager = null;

    public MidiFileLoader(
        BlockingQueue<MidiFileLoaderMessage> requestQueue,
        Map<String,String> environment
    ) {
        super("MIDI File Loader", requestQueue);

        requireNonNull(environment);
        this.environment = environment;
    }

    @Override
    public boolean startComponent() {
        this.minimalMidiResolution = Integer.parseInt(
            this.environment.getOrDefault(EnvironmentConstants.MIN_MIDI_RESOLUTION,"96")
        );

        this.outputManager = new OutputMqpManager(this.environment);

        try {
            this.outputManager.start();
            return true;
        } catch (UnknownHostException e) {
            return false;
        }
    }

    @Override
    public void stopComponent() {
        try {
            if(this.outputManager != null) {
                this.outputManager.requestTermination();
                this.outputManager.join();
            }
        } catch(Exception e) {
            Logger.error("Could not stop the output layer properly!");
            e.printStackTrace();
        }
    }

    @Override
    public void processRequest(MidiFileLoaderMessage message) {
        if(message instanceof LoadFileRequest) {
            LoadFileRequest request = (LoadFileRequest) message;
            handleLoadFileRequest(request);
        }
    }

    @Override
    public void sendTerminationRequestMessage() throws Exception {
        this.getComponentRequestQueue().put(new TerminateRequest());
    }

    private void handleLoadFileRequest(LoadFileRequest request) {
        try {
            byte[] content = FileUtils.readFileToByteArray(request.path.toFile());

            FileTypeDerivator ftd = new FileTypeDerivator(null, content);

            switch(ftd.type()) {

                case UNKNOWN:
                    Logger.error("Unknown content type was sent to the loader - content will be ignored!");
                    break;
                case MIDI:
                    loadMidiFile(content, request.originalFilename);
                    break;
                case SEQ_RAW:
                    loadRawSequenceFile(content, request.originalFilename);
                    break;
                case SEQ_GZ:
                    loadGzipedSequenceFile(content, request.originalFilename);
                    break;
            }

        } catch(IOException e) {
            Logger.error(String.format("Could not open input stream to [%s]",request.path.toAbsolutePath().toString()));
            e.printStackTrace();
        } catch (InterruptedException e) {
            Logger.error(String.format("Could not send request to the sequence controller! File: [%s]",request.originalFilename));
            e.printStackTrace();
        } catch (InvalidMidiDataException e) {
            Logger.error(String.format("Could not calculate the MIDI file correctly! File: [%s]",request.originalFilename));
            e.printStackTrace();
        }

    }

    private void loadGzipedSequenceFile(byte[] content, String originalFilename) throws IOException, InterruptedException, InvalidMidiDataException {
        // Decompress
        ByteArrayInputStream input = new ByteArrayInputStream(content);
        ByteArrayOutputStream output = new ByteArrayOutputStream();

        try (GZIPInputStream gis = new GZIPInputStream(input)) {
            byte[] buffer = new byte[1024];
            int len;
            while ((len = gis.read(buffer)) > 0) {
                output.write(buffer, 0, len);
            }
        }

        byte[] decopressedContent = output.toByteArray();

        // Load raw sequence file
        loadRawSequenceFile(decopressedContent, originalFilename);
    }

    private void loadRawSequenceFile(byte[] content, String originalFilename) throws InterruptedException, InvalidMidiDataException {
        // Check if ASCII representation is in use
        byte[] rawContent = content;
        if(FileTypeDerivator.getInstance(originalFilename, content).hasValidSeqAsciiHeader()) {
            AsciiSequenceToRawSequenceConverter asciiConverter = new AsciiSequenceToRawSequenceConverter(
                new ByteArrayInputStream(content)
            );
            rawContent = asciiConverter.convert().get();
        }

        RawSequenceToMidiConverter converter = new RawSequenceToMidiConverter(rawContent, this.minimalMidiResolution);
        Sequence midiFile = converter.convert();
        this.outputManager.processSequence(midiFile, originalFilename);
    }

    private void loadMidiFile(byte[] content, String originalFilename) throws InterruptedException {
        ByteArrayInputStream contentStream = new ByteArrayInputStream(content);
        if(originalFilename == null) { originalFilename = "!!! UNKNOWN !!!"; }
        try {
            Sequence midiFile = MidiSystem.getSequence(contentStream);
            this.outputManager.processSequence(midiFile, originalFilename);
        } catch (InvalidMidiDataException e) {
            Logger.error(String.format("MIDI File seems to be corrupt! [%s]",originalFilename));
            e.printStackTrace();
        } catch (IOException e) {
            Logger.error(String.format("Could not open input stream to [%s]",originalFilename));
            e.printStackTrace();
        }

    }

    public void requestStopLooping() throws InterruptedException {
        if(this.outputManager != null) {
            this.outputManager.requestStopLooping();
        }
    }
}
