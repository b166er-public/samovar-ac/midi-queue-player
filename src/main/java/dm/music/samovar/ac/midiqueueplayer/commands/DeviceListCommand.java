/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package dm.music.samovar.ac.midiqueueplayer.commands;

import dm.music.samovar.ac.midiqueueplayer.common.MidiDevicesSupport;
import org.tinylog.Logger;
import picocli.CommandLine;

import javax.sound.midi.MidiDevice;
import java.util.Map;
import java.util.concurrent.Callable;

@CommandLine.Command(
    name = "device-list",
    description = "List all MIDI devices accessible by your system",
    mixinStandardHelpOptions = true
)
public class DeviceListCommand implements Callable<Integer> {
    @Override
    public Integer call() throws Exception {
        Logger.info("Command [info] had been selected!");
        Logger.info("Following devices had been found ... ");

        Map<String, MidiDevice.Info> deviceMap = MidiDevicesSupport.getDeviceMap();

        for(String key : deviceMap.keySet())
        {
            Logger.info(
                String.format(
                    "ID [%s] -> NAME [%s]",
                    key,
                    deviceMap.get(key).getName()
                )
            );
        }

        Logger.info("No further MIDI devices had been found!");



        return 0;
    }
}
