/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package dm.music.samovar.ac.midiqueueplayer.input.fsmonitor;

import dm.music.samovar.ac.midiqueueplayer.common.FileTypeDerivator;
import dm.music.samovar.ac.midiqueueplayer.input.InputMqpComponent;
import dm.music.samovar.ac.midiqueueplayer.input.fsmonitor.internal.FileSystemMonitorThread;
import dm.music.samovar.ac.midiqueueplayer.input.fsmonitor.messages.FileSystemMonitorMessage;
import dm.music.samovar.ac.midiqueueplayer.input.fsmonitor.messages.NewMidiFileDetected;
import dm.music.samovar.ac.midiqueueplayer.input.fsmonitor.messages.TerminateRequest;
import dm.music.samovar.ac.midiqueueplayer.loader.messages.LoadFileRequest;
import dm.music.samovar.ac.midiqueueplayer.loader.messages.MidiFileLoaderMessage;
import org.apache.commons.io.FileUtils;
import org.tinylog.Logger;

import java.nio.file.Path;
import java.util.concurrent.BlockingQueue;

import static java.util.Objects.requireNonNull;

public class FileSystemMonitor extends InputMqpComponent<FileSystemMonitorMessage> {

    private final Path watchFolder;
    private final BlockingQueue<MidiFileLoaderMessage> midiLoaderQueue;

    private FileSystemMonitorThread monitoringThread = null;

    public FileSystemMonitor(
        BlockingQueue<FileSystemMonitorMessage> requestQueue,
        Path tmpFolder,
        BlockingQueue<MidiFileLoaderMessage> midiLoaderQueue,
        Path watchFolder
    ) {
        super("File System Monitor", requestQueue, tmpFolder);

        requireNonNull(watchFolder);
        requireNonNull(midiLoaderQueue);
        this.watchFolder = watchFolder;
        this.midiLoaderQueue = midiLoaderQueue;
    }


    @Override
    public boolean startComponent() {
        try {
            this.monitoringThread = new FileSystemMonitorThread(
                this.watchFolder,
                this.getComponentRequestQueue()
            );
            this.monitoringThread.start();
            Logger.info(
                String.format(
                    "Started FS endpoint [%s] !",
                    watchFolder.toAbsolutePath().toString()
                )
            );
        } catch(Exception e) {
            Logger.error("Could not start the monitoring thread!");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public void stopComponent() {
        try {
            this.monitoringThread.requestTermination();
            this.monitoringThread.join();
        } catch(Exception e) {
            Logger.error("Could not stop the path watch service!");
            e.printStackTrace();
        }
    }

    @Override
    public void processRequest(FileSystemMonitorMessage message) {
        if(message instanceof NewMidiFileDetected) {
            NewMidiFileDetected msgTyped = (NewMidiFileDetected) message;
            try {
                byte[] content = FileUtils.readFileToByteArray(msgTyped.path.toFile());

                FileTypeDerivator ftd = new FileTypeDerivator(
                    msgTyped.path.getFileName().toString(),
                    content
                );

                if(ftd.type() != FileTypeDerivator.Type.UNKNOWN) {
                    Path tmpPath = this.storeToTmp(
                        content,
                        ftd.extension()
                    );
                    Logger.info(
                        String.format(
                            "Created new [%s] file in tmp folder [%s] !",
                            ftd.extension(),
                            tmpPath.toAbsolutePath().toString()
                        )
                    );
                    this.midiLoaderQueue.put(
                        new LoadFileRequest(
                            tmpPath,
                            msgTyped.path.getFileName().toString()
                        )
                    );
                } else {
                    Logger.error("File [%s] seems to be invalid and will be ignored!");
                }
            } catch(Exception e) {
                Logger.error(
                    String.format(
                        "Could not load content from path [%s] !",
                        msgTyped.path.toAbsolutePath().toString()
                    )
                );
                e.printStackTrace();
            }
        }
    }

    @Override
    public void sendTerminationRequestMessage() throws InterruptedException {
        this.getComponentRequestQueue().put(new TerminateRequest());
    }
}