/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package dm.music.samovar.ac.midiqueueplayer.output.sequencectrl;

import dm.music.samovar.ac.midiqueueplayer.output.sequencectrl.internal.MidiDeviceReceiverInterceptor;
import dm.music.samovar.ac.midiqueueplayer.output.sequencectrl.messages.MidiSequencerControllerMessages;
import dm.music.samovar.ac.midiqueueplayer.output.sequencectrl.messages.PlaySequenceRequest;
import dm.music.samovar.ac.midiqueueplayer.output.sequencectrl.messages.StopLoopRequest;
import dm.music.samovar.ac.midiqueueplayer.output.sequencectrl.messages.TerminateRequest;
import dm.music.samovar.ac.midiqueueplayer.common.MidiDevicesSupport;
import dm.music.samovar.ac.midiqueueplayer.common.MqpComponent;
import org.tinylog.Logger;

import javax.sound.midi.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import static java.util.Objects.requireNonNull;

public class MidiSequencerController extends MqpComponent<MidiSequencerControllerMessages> implements MetaEventListener {

    private static final int END_OF_TRACK_TYPE = 0x2F;

    private final String targetDeviceId;

    private MidiDevice targetDeviceHandler = null;
    private MidiDeviceReceiverInterceptor targetDeviceReceiver = null;
    private Sequencer sequencerHandler = null;

    private BlockingQueue<PlaySequenceRequest> playQueue = new LinkedBlockingQueue<>();

    public MidiSequencerController(
        BlockingQueue<MidiSequencerControllerMessages> requestQueue,
        String targetDeviceId
    ) {
        super("MIDI Sequencer Controller", requestQueue);
        requireNonNull(targetDeviceId);
        this.targetDeviceId = targetDeviceId;
    }

    @Override
    public boolean startComponent() {
        try {
            MidiDevice.Info deviceInfo = MidiDevicesSupport.getDeviceById(targetDeviceId);

            targetDeviceHandler = MidiSystem.getMidiDevice(deviceInfo);
            targetDeviceHandler.open();

            // This brainfuck with my interceptor is needed so that the sequencer do not always
            // send all notes off after I reset the tick position to 0.
            targetDeviceReceiver = new MidiDeviceReceiverInterceptor(
                targetDeviceHandler.getReceiver()
            );
            sequencerHandler = MidiSystem.getSequencer(false);
            sequencerHandler.getTransmitter().setReceiver(targetDeviceReceiver);

            sequencerHandler.addMetaEventListener(this);
            sequencerHandler.open();
            targetDeviceReceiver.deactivateChannelModeMessages();
        } catch(Exception e) {
            Logger.error(
                String.format(
                    "Could not open MIDI device [%s] properly!",
                    targetDeviceId
                )
            );
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public void stopComponent() {
        targetDeviceReceiver.activateChannelModeMessages();
        if(sequencerHandler != null)
        {
            sequencerHandler.stop();
            sequencerHandler.close();
            sequencerHandler = null;
        }

        if(targetDeviceReceiver != null)
        {
            targetDeviceReceiver.close();
            targetDeviceReceiver = null;
        }

        if(targetDeviceHandler != null)
        {
            targetDeviceHandler.close();
            targetDeviceHandler = null;
        }
    }

    @Override
    public void processRequest(MidiSequencerControllerMessages message) {
        if(message instanceof PlaySequenceRequest) {
            PlaySequenceRequest typedMessage = (PlaySequenceRequest) message;
            playSequence(typedMessage);
        } else if(message instanceof StopLoopRequest) {
            sequencerHandler.stop();
        }
    }

    @Override
    public void sendTerminationRequestMessage() throws Exception {
        this.getComponentRequestQueue().put(new TerminateRequest());
    }

    private void playSequence(PlaySequenceRequest message) {
        try {
            addEndOfTrackIfNeeded(message.content);

            if(!sequencerHandler.isRunning()) {
                sequencerHandler.setSequence(message.content);
                sequencerHandler.setTickPosition(0);
                sequencerHandler.setLoopCount(0);
                sequencerHandler.start();
            } else {
                playQueue.put(message);
            }


        } catch (InterruptedException e) {
            Logger.error(
                String.format(
                    "Could not add [%s] to playlist!",
                    message.originalFilename
                )
            );
            e.printStackTrace();
        } catch (InvalidMidiDataException e) {
            Logger.error(
                String.format(
                    "Could not set the sequencer for [%s] !",
                    message.originalFilename
                )
            );
            e.printStackTrace();
        }
    }

    private void addEndOfTrackIfNeeded(Sequence sequence) throws InvalidMidiDataException {

        // Is there an "END OF TRACK" event?
        boolean endOfTrackEventFound = false;
        Track mainTrack = sequence.getTracks()[0];
        int amountOfEvents = mainTrack.size();
        for(int e = 0; e < amountOfEvents; e++) {
            MidiMessage msg = mainTrack.get(e).getMessage();
            if(msg instanceof MetaMessage) {
                MetaMessage msgTyped = (MetaMessage) msg;
                if(msgTyped.getType() == END_OF_TRACK_TYPE) {
                    endOfTrackEventFound = true;
                    break;
                }
            }
        }

        // If the is no END OF TRACK event add one in the end
        if(!endOfTrackEventFound) {
            Logger.info("Add END OF TRACK event to sequence!");
            long lastTick = sequence.getTickLength();
            MetaMessage endOfTrack = new MetaMessage(END_OF_TRACK_TYPE, null, 0);
            MidiEvent endOfTrackEvent = new MidiEvent(endOfTrack,lastTick);
            sequence.getTracks()[0].add(endOfTrackEvent);
        }
    }

    @Override
    public void meta(MetaMessage meta) {
        if(meta.getType() == END_OF_TRACK_TYPE) {
            Logger.info("End of track reached!");

            boolean restartSequencer = true;
            try {
                if(!this.playQueue.isEmpty()) {
                    PlaySequenceRequest nextSequenceMessage = this.playQueue.take();
                    sequencerHandler.setSequence(nextSequenceMessage.content);
                    Logger.info(
                        String.format(
                            "The sequence [%s] is now playing by the sequencer!",
                            nextSequenceMessage.originalFilename
                        )
                    );
                } else {
                    Logger.info("Replay current sequence!");
                }
            } catch(Exception e) {
                Logger.info("Could not transfer new sequence to the sequencer!");
                e.printStackTrace();
                restartSequencer = false;
                sequencerHandler.stop();
            }

            if(restartSequencer) {
                sequencerHandler.setTickPosition(0);
                sequencerHandler.setLoopCount(0);
                sequencerHandler.start();

            }
        }
    }
}
