/*
 * Copyright (C) 2021 Daniil Moerman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package dm.music.samovar.ac.midiqueueplayer.common;

import java.util.HashMap;
import java.util.Map;
import static java.util.Objects.requireNonNull;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import org.apache.commons.codec.digest.DigestUtils;

public class MidiDevicesSupport {

    private static String deriveIdFromName(String name)
    {
        return DigestUtils.md5Hex(name).toUpperCase().substring(0,4);
    }

    public static Map<String,MidiDevice.Info> getDeviceMap() throws MidiUnavailableException
    {
        Map<String,MidiDevice.Info> result = new HashMap<>();

        MidiDevice.Info[] deviceInfoList = MidiSystem.getMidiDeviceInfo();

        for(int n = 0; n < deviceInfoList.length; n++)
        {
            MidiDevice.Info info = deviceInfoList[n];

            MidiDevice device = MidiSystem.getMidiDevice(info);

            String name = Integer.toString(n) + " " + info.getName();
            String id = deriveIdFromName(name);
            result.put(id, info);
        }

        return result;

    }

    public static MidiDevice.Info getDeviceById(String id) throws MidiUnavailableException
    {
        requireNonNull(id);
        Map<String, MidiDevice.Info> deviceMap = getDeviceMap();
        MidiDevice.Info device = deviceMap.get(id);
        if(device != null)
        {
            return device;
        }
        else
        {
            throw new RuntimeException("No MIDI device with id [ " + id + " ] found!");
        }
    }
}
